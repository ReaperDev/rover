<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RoverController extends Controller
{
    public $existingPositions = '';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Takes an input file from a post request and processes it for positioning and movements
     * of the rovers
     *
     * @param Request $request
     * @return false|string
     */
    public function processFile(Request $request) {
        $fileName = $request->file;
        $request->file->move('/tmp/', $fileName);

        file_put_contents('/tmp/rover.output', "");
        $gridH = 0;
        $gridW = 0;
        $ln = 1;
        $rover = 1;

        $starting = [];
        $handle = fopen( $fileName, "r");
        $moves = [];
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                $line = preg_replace( "/\r|\n/", "", $line);
                $roverName = 'cg' . $rover;
                $moves[$roverName] = [];
                if ($ln == 1) {
                    $contraints = explode(' ', $line);
                    $gridW = $contraints[0];
                    $gridH = $contraints[1];
                } else {
                    if (count($starting) == 0) {
                        $starting = explode(' ', $line);
                    } else {
                        $moves[$roverName]['position'] = $starting;
                        $moves[$roverName]['moves'] = $line;
                        $endingPosition = $this->moveRover($starting, $line, $gridW, $gridH);
                        if (!$endingPosition)
                            return '{"success": "false", "message": "Rover damage will occur with input movements."}';
                        $this->existingPositions .= $endingPosition . " - ";
                        $moves[$roverName]['endingPosition'] = $endingPosition;
                        $starting = [];
                        $rover++;
                    }
                }
                $ln++;
            }
            $output = file_get_contents('/tmp/rover.output');
            fclose($handle);
            unlink($fileName);
            if($output == '')
                return '{"success": "false", "message": "Please check input file and try again."}';
            return json_encode(['width' => $gridW, 'height' => $gridH, 'output' => $output, 'rovers' => $moves]);
        } else {
            return 'problem opening file';
        }
    }

    /**
     * Moves the rover
     *
     * @param $starting
     * @param $movement
     * @param $maxX
     * @param $maxY
     * @return false|string
     */
    public function moveRover($starting, $movement, $maxX, $maxY) {
        $x = $starting[0];
        $y = $starting[1];
        $d = $starting[2];
        $moves = str_split($movement);
        foreach ($moves as $move) {
            if ($move == 'L') {
                if ($d == 'N')
                    $d = 'W';
                elseif ($d == 'W')
                    $d = 'S';
                elseif ($d == 'S')
                    $d = 'E';
                else
                    $d = 'N';
            } elseif ($move == 'R') {
                if ($d == 'N')
                    $d = 'E';
                elseif ($d == 'E')
                    $d = 'S';
                elseif ($d == 'S')
                    $d = 'W';
                else
                    $d = 'N';
            } else {
                if ($d == 'N') {
                    $y++;
                } elseif ($d == 'E') {
                    $x++;
                } elseif ($d == 'S') {
                    $y--;
                } else {
                    $x--;
                }
                if($this->checkPosition($x, $y, $maxX, $maxY)) {
                    return false;
                }
            }
        }
        file_put_contents('/tmp/rover.output', "$x $y $d\n", FILE_APPEND);
        return "$x $y $d";
    }

    /**
     * Checks the positioning of a rover to make sure it will not exceed the coordinates of the grid
     * or collide with another rover
     *
     * @param $x
     * @param $y
     * @param $maxX
     * @param $maxY
     * @return bool
     */
    public function checkPosition($x, $y, $maxX, $maxY) {
        if ($x > $maxX)
            return true;
        if ($x < 0)
            return true;
        if ($y > $maxY)
            return true;
        if ($y < 0)
            return true;
        if (strpos($this->existingPositions, $x . ' ' . $y) > -1)
            return true;
        return false;
    }
}
